#ifndef LOGICAL_H
#define LOGICAL_H

#include <iostream>
#include <vector>
#include <sstream>
#include <algorithm> //transform  ::tolower
#include "EncDec.h"
using namespace std;

class Logical : public EncDec {
	public:
		Logical(string data, string fN);
		~Logical();
		void compress();
		void decompress();
		void setData(string);
		void setFile(string);
	protected:		
		void dateFormat(vector<string>&);
	private:
		string monthAlphaToNum(string); //convert month to numeric
		string monthNumToAlpha(int); //convert month to alphabetic
};

#endif
