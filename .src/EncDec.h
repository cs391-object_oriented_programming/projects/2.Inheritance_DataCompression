#ifndef ENCDEC_H
#define ENCDEC_H
#include <iostream>
#include <vector>
#include <fstream>
using namespace std;

class EncDec {
	public:
		EncDec(string,string);
		~EncDec();
		void action(char act); //'c' compress, 'd' decompress
	protected:
		virtual void fileIO(char); //read and write to file
		/*Pure Virtuals*/
		virtual void compress()=0;
		virtual void decompress()=0;
		/*Setters*/
		void setHolder(string);
		void setFileName(string);
		/*Getters*/
		string getFileName();
		string getHolder();
		/*Variables*/
		string fileName;
		string strHolder;
};

#endif
