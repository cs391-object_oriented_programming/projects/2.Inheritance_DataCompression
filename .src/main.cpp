#include <iostream>
#include "EncDec.h"
#include "Logical.h"
#include "RunLength.h"
using namespace std;

void modValues(string&,string&,int&);

int main(int argc, char** argv) {
	cout<<"\t############################################\n"
	    <<"\t### CS 391 String Compressor/Decompressor ##\n"
	    <<"\t###          Jose Madureira               ##\n"
	    <<"\t############################################\n";

	bool quit= false;
	int type= 0;
	string file= "", data= "";

	/*Get the data*/
	cout<<"\n  Enter the data to compress: ";
	getline(cin,data);

	/*Choose compression*/
	do {
		cout<<"  What type of data is it?"
		    <<"\n\t(1) Date (Logical will be used)"
		    <<"\n\t(2) Character Repetion (Run Length)"
		    <<"\n\t(3) "
			<<"\n>>choice: ";
		cin>>type;
	} while(type<1 || type>3);
	cin.get();//eat the newline

	/*Get the file name*/
	cout<<"  Enter the file name where the data is to be saved: ";
	getline(cin,file);


	/* TODO (#1#): IMPLEMENT COMPRESSION OBJECT TYPE CHANGE AT RUNTIME */
//	bool reset= 0; //will create new object if triggered
//
//	if(type==1) {
//		RunTime *RT= new RunTime(data,file);
//		RT->setData(data);
//		RT->setFile(file);
//	} else if(type==2) {
//
//	} else if(type==3) {
//
//	} else cout<<"\n\t/!\\ERROR: Invalid data type exception /!\\"<<endl;


	Logical *LC= new Logical(data,file);
	RunLength *RL= new RunLength(data,file);

	/*Operate on the data*/
	do {
		cout<<"\n\t\t## Main Menu ##"
//		    <<"\n  String data: "<<data
		    <<"\n\t(1) Compress"
		    <<"\n\t(2) Decompress"
		    <<"\n\t(3) Change the string and/or file name"
		    <<"\n\t(q) Quit Program"
		    <<"\n>>choice: ";

		char menuChoice= ' ';
		do {
			cin>>menuChoice;
			if(menuChoice=='q') return 0; //quit the program
		} while(menuChoice!='1' && menuChoice!='2' && menuChoice!='3');

		switch(menuChoice) {
			case '1':
				if(type==1) LC->compress();
				if(type==2) RL->compress();

				break;
			case '2':
				if(type==1)	LC->decompress();
				if(type==2) RL->decompress();


				break;
			case '3':
				modValues(data,file,type);
				
				if(type==1) {
					LC->setData(data);
					LC->setFile(file);
				}else if(type==2){
					RL->setData(data);
					RL->setFile(file);
				}
				

				break;
			case 'q':
				quit= true;
				break;
			default:
				cout<<"\n\t!Invalid Menu selection!\n";
				break;
		}
	} while(quit!=true);

	delete LC;
	delete RL;
	
	return 0;
}

void modValues(string &data, string &file, int &type) {
	char ch=' ';

	do { //allow multiple changes

		do { //force valid option
			cout<<"\n  What do you want to change?"
			    <<"\n\t(1) The data"
			    <<"\n\t(2) The file name"
			    <<"\n\t(q) Return to main menu\n";
			cin>>ch;
			cin.get(); //eat the newline

			if(ch=='q' || ch =='Q') break;
		} while(ch!='1' && ch!='2');

		if(ch=='1') {
			cout<<">>new data: ";
			getline(cin,data);
			cout<<"  What type of data is it?"
			    <<"\n\t(1) Date (Logical will be used)"
			    <<"\n\t(2) Character Repetion (Run Length)"
			    <<"\n\t(3) ";
			cin>>type;
		} else if(ch=='2') {
			cout<<">>new file name: ";
			getline(cin,file);
		}else break;

	} while(ch!='q' && ch!='Q');
}
