#include "EncDec.h"

EncDec::EncDec(string data, string fName) {
	strHolder= data;
	fileName= fName;
}

EncDec::~EncDec() {

}

void EncDec::fileIO(char rw) {
	if(rw=='r'||rw=='R') { //read file
		string text;
		ifstream ifs;
		ifs.open(fileName);
		ifs>> strHolder;
		ifs.close();
		cout<<"\n\tSTATUS: *File Read*\n";
	} else if(rw=='w'||rw=='W') { //write to file
		ofstream ofs(fileName);
		ofs<< strHolder;
		ofs.close();
		cout<<"\n\tSTATUS: *File Saved*\n";
	} else {
		cout<<"\n\t/!\\ ERROR! Received neither file READ nor WRITE.\n";
	}
}
void EncDec::setHolder(string hold) {
	strHolder= hold;
}

void EncDec::setFileName(string fn) {
	fileName= fn;
}

string EncDec::getFileName() {
	return fileName;
}

string EncDec::getHolder() {
	return strHolder;
}
