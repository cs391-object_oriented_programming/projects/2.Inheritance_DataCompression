#include "Logical.h"

Logical::Logical(string data, string fN) : EncDec(data,fN) {

}

Logical::~Logical() {

}

void Logical::setData(string data) {
	setHolder(data);
}

void Logical::setFile(string file) {
	setFileName(file);
}

void Logical::compress() {
	int dmy;
	/*Set date format*/
	vector<string> vDate;
	dateFormat(vDate);

	string moYN;
	cout<<"\n  Are the months alphabetic (December/dec)?   ";
	cin>>moYN;

	/*Convert month from letters to numbers*/
	if(moYN.at(0)=='y' || moYN.at(1)=='Y') {
		string temp= monthAlphaToNum(vDate.at(1)); //convert month to padded number
		vDate[1]= temp;
	}

	string year= vDate.at(0),
	       month= vDate.at(1),
	       day= vDate.at(2);

	if(year.length()>2) year= year.substr(2,4); //remove leading '20**'
	if(month.length()==1) month.insert(0,"0");	//pad month '0*'
	if(day.length()==1) day.insert(0,"0");		//pad day '0*'

	/*Encode string with 'yymmdd' format and save to file*/
	strHolder= year+month+day;
	cout<<"\n\tSTATUS: *Date was encoded*";
	fileIO('w');

//	/* Detect Year (200* or 201* )  with space after*/
//	if(strHolder.at(0)=='2' && strHolder.at(1)=='0' &&
//	        (strHolder.at(2)=='0' || strHolder.at(2)=='1') &&  strHolder.at(3)==' ') {]
//		year= strHolder.substr(0,3);
//	}
}

/* TODO (#1#): IMPLEMENT CONVERSION FROM NUMBER TO STRING*/
void Logical::decompress() {
	string row= "", month="", year="", day="";
	int moNum=0;

	/*Read compressed string*/
	fileIO('r');

	/*Split the string into parts*/
	row= getHolder();
	year= 	"20"+row.substr(0,2);
	month=	row.substr(2,2);
	day=	row.substr(4,2);

	/*Convert month to letters*/
	moNum= stoi(month);
	moNum= (moNum+1)-1; //remove leading 0 if it exists
	month= monthNumToAlpha(moNum);
	
	cout<<"\tThe decompressed date is: "<<day<<" "<<month<<" "<<year<<endl;

//	cout<<"\n\tMONTH: "<<month<<endl;
//	cout<<"\tYUAR: "<<year<<endl;
//	cout<<"\tDAY: "<<day<<endl;
}

void Logical::dateFormat(vector<string> &vDate) {
	int dmy;

	cout<<"\n  How is the date formatted?"
	    <<"\n\t(1) Day Month Year (ex: 20 dec 2016)"
	    <<"\n\t(2) Month Day Year (ex: december 20 2016)"
	    <<"\n\t(3) Year Month Day (ex: 2016 december 20)"
	    <<"\n>>Choice: ";
	cin>>dmy;
	while(dmy<1 || dmy>3) {
		cout<<"\n\t!Invalid Choice!\n";
		cin>>dmy;
	}

	/*Split the full date string*/
	string year, month, day;
	istringstream iss(strHolder);

	if(dmy==1) 		iss >> day >> month >> year;
	else if(dmy==2)	iss >> month >> day >> year;
	else if(dmy==3) iss >> year >> month >> day;

	vDate.push_back(year);
	vDate.push_back(month);
	vDate.push_back(day);
}

//REF: https://notfaq.wordpress.com/2007/08/04/cc-convert-string-to-upperlower-case/
string Logical::monthAlphaToNum(string mo) {
	string sub="", num= "";
	sub= mo.substr(0,3);

	/*Convert the first 3 letters to lower case*/
	transform(sub.begin(),sub.end(),sub.begin(),::tolower);

	/*Compare the first letter*/
	switch(sub.at(0)) {
		case 'j' : //jan & jun & jul
			(sub.at(2)=='n') ? num= "06" : num= "07";
			if(sub.at(1)=='a') num= "01"; //january
			break;
		case 'f' : //feb
			num= "02";
			break;
		case 'm' : //mar & may. Compare the 3rd letter
			(sub.at(2)=='r') ? num= "03" : num= "05";
			break;
		case 'a': //apr & aug. Compare the 2nd letter
			(sub.at(1)=='p') ? num= "04" : num= "08";
			break;
		case 's' : //sep
			num= "09";
			break;
		case 'o' : //oct
			num= "10";
			break;
		case 'n' : //nov
			num= "11";
			break;
		case 'd' : //dec
			num= "12";
			break;
		default:
			cout<<"\n\t/!\\ No valid alphabetic month received /!\\\n";
			break;
	}
	return num;
}

string Logical::monthNumToAlpha(int month) {
	vector <string> vMonths {"January","February","March","April","May","June","July"
	                         ,"August","September","October","November","December"
	                        };
	string expanded= "";
	int match= month;

	for(int mo=0; mo<12; ++mo) {
		if(mo+1 == match) expanded= vMonths.at(mo);
	}
	return expanded;
}

