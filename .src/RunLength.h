#ifndef RUNLENGTH_H
#define RUNLENGTH_H

#include <iostream>
#include <vector>
#include <cstring>
//#include <sstream>
#include "EncDec.h"
using namespace std;

class RunLength : public EncDec {
	public:
		RunLength(string, string);
		~RunLength();
		void compress();
		void decompress();
		void setData(string);
		void setFile(string);
};

#endif
